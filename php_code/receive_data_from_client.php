<?php

include_once("sql_connect.php");//include the sql_connect.php file for database connection

$sample_time = " ";//initialize the sample_time variable
$id = " ";//initialize the id variable


if ($_SERVER['REQUEST_METHOD'] == 'POST') {//Check it is coming from a form
    $json = file_get_contents('php://input');//get the json data
    $data = json_decode($json);//decode the json data
    $id = $data->id;//get the id from the json data
    if($id == "1"){//check if the id is 1
        $sample_time = $data->sample_time;//get the sample_time from the json data
        $sample_time = $sample_time*1000;//convert the sample_time to milliseconds
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
            
        $sql = "UPDATE sample_time_table SET sample_time = '$sample_time' WHERE id = 1";//update the sample_time in the database
    }
    if($id == "2"){//check if the id is 2
        $min_temp = $data->temp_min;//get the min_temp from the json data
        $max_temp = $data->temp_max;//get the max_temp from the json data
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
            
        $sql = "UPDATE alarm_parameters SET temp_min = '$min_temp' ,temp_max = '$max_temp' WHERE id = 1";
        //$sql = "UPDATE alarm_parameters SET temp_max = '$max_temp' WHERE id = 2";
    }
    if($id == "3"){//check if the id is 2
        $min_humid = $data->humid_min;//get the min_temp from the json data
        $max_humid = $data->humid_max;//get the max_temp from the json data
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
            
        $sql = "UPDATE alarm_parameters SET humid_min = '$min_humid' ,humid_max = '$max_humid' WHERE id = 1";
        //$sql = "UPDATE alarm_parameters SET temp_max = '$max_temp' WHERE id = 2";
    }


    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } 
    else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    //$conn->close();
    }
else {
    echo "No data posted with HTTP POST.";
}


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>