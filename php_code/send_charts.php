<?php

include_once("sql_connect.php");//include the sql_connect.php file for database connection

    $id = "SELECT id FROM time_period WHERE idx = 1";   //select the id from the database

    $result2 = $conn->query($id);   //execute the query
    $date2 = array();   //initialize the array
    while ($row2 = $result2->fetch_assoc()) {   //fetch the data from the database
        $date2[] = $row2;   //put the data in the array
    }

    if($date2[0]["id"] == 1){   //check if the id is 1
        $sql = "SELECT * FROM tabel WHERE timp > (NOW() - INTERVAL 1 DAY)"; //select the data from the last day
    }

    if($date2[0]["id"] == 2){   //check if the id is 2
        $sql = "SELECT * FROM tabel WHERE timp > (NOW() - INTERVAL 1 WEEK)";    //select the data from the last week
    }
    if($date2[0]["id"] == 3){   //check if the id is 3
        $sql = "SELECT * FROM tabel WHERE timp > (NOW() - INTERVAL 1 MONTH)";   //select the data from the last month
    }
    $result = $conn->query($sql);   //execute the query

    $date = array();        //initialize the array
    while ($row = $result->fetch_assoc()) { //fetch the data from the database
        $date[] = $row; //put the data in the array

    }
    echo json_encode($date);//encode the array in json format

    mysqli_close($conn);//close the connection
?>
