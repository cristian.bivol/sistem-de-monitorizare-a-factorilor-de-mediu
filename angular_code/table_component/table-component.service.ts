import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TableComponent } from './table-component';

@Injectable({
    providedIn: 'root',
})


export class TableComponentService {
    private url = 'http://localhost/send_table.php';

    constructor(private http: HttpClient) {}

    getData(): Observable<TableComponent[]>{
        return this.http.get<TableComponent[]>(this.url);
    }

}