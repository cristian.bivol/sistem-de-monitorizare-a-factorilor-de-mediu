import { Component, OnInit } from '@angular/core';
import { TableComponentService } from './table-component.service';
import { TableComponent } from './table-component';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, timer } from 'rxjs';


@Component({
  selector: 'app-user-table',
  styleUrls: ['./table-component.component.css'],
  templateUrl: './table-component.component.html'
})

export class UserTableComponent implements OnInit {
  //dataSource = new MatTableDataSource<TableComponent>();
  url = 'http://localhost/send_table.php';
  //datasource: TableComponent | undefined;
  dataSource: TableComponent[] = [];
  dataSource$!: Observable<TableComponent[]>;

  displayedColumns: string[] = ['temperatura', 'umiditate', 'co2' ,'timp'];
   
  constructor(private tableService: TableComponentService) { }

  ngOnInit() {
    //this.getTableDataSubscibe();
    //timer(0,5000).subscribe(() => this.getTableDataAsyncPipe());
  }

  public getTableDataSubscibe() {
    this.tableService.getData().subscribe((data:TableComponent[]) => {this.dataSource = data});
  }

  public getTableDataAsyncPipe() {
    this.dataSource$ = this.tableService.getData();
  }

  onSave(event?: MouseEvent) {
    this.getTableDataSubscibe();
  }
  
}

