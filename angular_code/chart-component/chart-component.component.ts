import { Component,AfterViewInit, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { timer } from 'rxjs';

@Component({
  selector: 'app-chart-component',
  templateUrl: './chart-component.component.html',
  styleUrls: ['./chart-component.component.css']
})
export class ChartComponentComponent implements AfterViewInit, OnInit {
  dataPointsTemperature:any = []; // the data that will be displayed on the chart
  dataPointsHumidity:any = [];
  dataPointsCO2:any = [];
  chart:any = [];

  selected = '';// the default value of the select
  temp: number = 0;       // the input converted to number


  max_temperature: number = 0;
  min_temperature: number = 0;
  max_humidity: number = 0;
  min_humidity: number = 0;

  save_time_period(){

	this.temp = Number.parseInt(this.selected);
	this.addTimePeriod(JSON.stringify({ id:this.selected}));

  }

  addTimePeriod(time_period:string): void { // add the sample time to the database
    this.http.post('http://localhost/receive_time_period.php',time_period).subscribe();// send the sample time to the database
    console.log("The sample time is added to the database");// log the message to the console
  }


  toolTip = {
		shared: true
	};
	legend = {
		cursor: "pointer",
		itemclick: function (e: any) {
		  if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		  } else {
			e.dataSeries.visible = true;
		  }
		  e.chart.render();
		}
	};

  constructor(private http : HttpClient) { }
  chartOptionsTemperature = {
    theme: "light2",
    zoomEnabled: true,
    exportEnabled: true,
    title: {
      text:"Temperature Chart"
    },
    toolTip: this.toolTip,
    axisY: {
      title: "Temperature [in °C]",
      suffix: "°C",
	  stripLines:[
		{                
			color:"#d8d8d8"
		}
		]
    },
    data: [{
      type: "line",
      name: "Temperature [in °C]",
      yValueFormatString: "#,###.00'°C'",
      xValueType: "dateTime",
      dataPoints: this.dataPointsTemperature
    }]
  }

  chartOptionsHumidity = {
    theme: "light2",
    zoomEnabled: true,
    exportEnabled: true,
    title: {
      text:"Humidity Chart"
    },
    toolTip: this.toolTip,
    axisY: {
      title: "Humidity [in %]",
      suffix: " %",
	  stripLines:[
		{              
			color:"#d8d8d8"
		}
		]
    },
    data: [{
      type: "line",
      name: "Humidity [in %]",
      yValueFormatString: "#,###.00'%'",
      xValueType: "dateTime",
      dataPoints: this.dataPointsHumidity
    }]
  }

  chartOptionsCO2 = {
    theme: "light2",
    zoomEnabled: true,
    exportEnabled: true,
    title: {
      text:"CO2 Concentration Chart"
    },
    toolTip: this.toolTip,
    axisY: {
      title: "CO2 [in ppm]",
      suffix: " ppm",
	  stripLines:[
		{              
			value:1000,
			thickness:2,
			lineDashType:"dash",
			color:"red"
		}
		]
    },
    data: [{
      type: "line",
      name: "CO2 [in ppm]",
      yValueFormatString: "#,###.00'ppm'",
      xValueType: "dateTime",
      dataPoints: this.dataPointsCO2
    }]
  }

  getChartInstance(chart: object) {
    //this.chart = chart;
	this.chart.push(chart);
  }
  
  ngOnInit() {
	timer(0,2000).subscribe(() => this.get_alarm_parameters());
	this.get_data();
  }

  get_data(){
	this.http.get('http://localhost/send_charts.php', { responseType: 'json' }).subscribe((response: any) => {
		let data = response;
		for(let i = 0; i < data.length; i++){
		  this.dataPointsTemperature.push({x: new Date(data[i].timp), y: Number(data[i].temperatura) });
		  this.dataPointsHumidity.push({x: new Date(data[i].timp), y: Number(data[i].umiditate) });
		  this.dataPointsCO2.push({x: new Date(data[i].timp), y: Number(data[i].co2) });
		}
		this.chart[0].render();
		this.chart[1].render();
		this.chart[2].render();
	  });
  }

  update_alarm(charts:any, min_temperature:number, max_temperature:number, min_humidity:number, max_humidity:number){
	charts[0].options.axisY.stripLines[0].startValue = min_temperature;
	charts[0].options.axisY.stripLines[0].endValue = max_temperature;
	charts[1].options.axisY.stripLines[0].startValue = min_humidity;
	charts[1].options.axisY.stripLines[0].endValue = max_humidity;
  }

  ngAfterViewInit() {
		this.syncCharts(this.chart, true, true, true);
	}
 
	syncCharts = (charts: any, syncToolTip: any, syncCrosshair: any, syncAxisXRange: any) => {
		if(!this.onToolTipUpdated){
		  this.onToolTipUpdated = function(e: any) {
			for (var j = 0; j < charts.length; j++) {
			  if (charts[j] != e.chart)
				charts[j].toolTip.showAtX(e.entries[0].xValue);
			}
		  }
		}
 
		if(!this.onToolTipHidden){
		  this.onToolTipHidden = function(e: any) {
			for( var j = 0; j < charts.length; j++){
			  if(charts[j] != e.chart)
				charts[j].toolTip.hide();
			}
		  }
		}
 
		if(!this.onCrosshairUpdated){
		  this.onCrosshairUpdated = function(e: any) {
			for(var j = 0; j < charts.length; j++){
			  if(charts[j] != e.chart)
				charts[j].axisX[0].crosshair.showAt(e.value);
			}
		  }
		}
 
		if(!this.onCrosshairHidden){
		  this.onCrosshairHidden =  function(e: any) {
			for( var j = 0; j < charts.length; j++){
			  if(charts[j] != e.chart)
				charts[j].axisX[0].crosshair.hide();
			}
		  }
		}
 
		if(!this.onRangeChanged){
		  this.onRangeChanged = function(e: any) {
			for (var j = 0; j < charts.length; j++) {
			  if (e.trigger === "reset") {
				charts[j].options.axisX.viewportMinimum = charts[j].options.axisX.viewportMaximum = null;
				charts[j].options.axisY.viewportMinimum = charts[j].options.axisY.viewportMaximum = null;
				charts[j].render();
			  } else if (charts[j] !== e.chart) {
				charts[j].options.axisX.viewportMinimum = e.axisX[0].viewportMinimum;
				charts[j].options.axisX.viewportMaximum = e.axisX[0].viewportMaximum;
				charts[j].render();
			  }
			}
		  }
		}
 
		for(var i = 0; i < charts.length; i++) { 
 
		  //Sync ToolTip
		  if(syncToolTip) {
			if(!charts[i].options.toolTip)
			  charts[i].options.toolTip = {};
 
			charts[i].options.toolTip.updated = this.onToolTipUpdated;
			charts[i].options.toolTip.hidden = this.onToolTipHidden;
		  }
 
		  //Sync Crosshair
		  if(syncCrosshair) {
			if(!charts[i].options.axisX)
			  charts[i].options.axisX = { labelAngle: 0, crosshair: { enabled: true, snapToDataPoint: true, valueFormatString: "HH:mm" }};
			
			charts[i].options.axisX.crosshair.updated = this.onCrosshairUpdated; 
			charts[i].options.axisX.crosshair.hidden = this.onCrosshairHidden; 
		  }
 
		  //Sync Zoom / Pan
		  if(syncAxisXRange) {
			charts[i].options.zoomEnabled = true;
			charts[i].options.rangeChanged = this.onRangeChanged;
		  }
		  
		  charts[i].render();
		}
	}

  onToolTipUpdated: any; onToolTipHidden: any; onCrosshairUpdated: any; onCrosshairHidden: any; onRangeChanged: any;

  get_alarm_parameters(): void{
    this.http.get('http://localhost/send_alarm_parameters.php', { responseType: 'json' }).subscribe((response: any) => {
    let data = response;
	this.min_temperature = Number(data[0].temp_min);
	this.max_temperature = Number(data[0].temp_max);
	this.min_humidity = Number(data[0].humid_min);
	this.max_humidity = Number(data[0].humid_max);

	this.update_alarm(this.chart, this.min_temperature, this.max_temperature, this.min_humidity, this.max_humidity);
	this.chart[0].render();
	this.chart[1].render();

    });
  }


}
