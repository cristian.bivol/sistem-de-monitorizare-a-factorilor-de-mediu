import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChartComponent } from './chart-component';

@Injectable({
    providedIn: 'root',
})


export class ChartComponentService {
    private url = 'http://localhost/send_table.php';

    constructor(private http: HttpClient) {}

    getData(): Observable<ChartComponent[]>{
        return this.http.get<ChartComponent[]>(this.url);
    }

}