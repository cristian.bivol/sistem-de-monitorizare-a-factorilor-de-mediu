import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MatTabsModule } from '@angular/material/tabs'; 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabComponentComponent } from './tab-component/tab-component.component';

import { HttpClientModule } from '@angular/common/http';

import { UserTableComponent } from './table-component/table-component.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';


import { ChartComponentComponent } from './chart-component/chart-component.component';
import { CanvasJSAngularChartsModule } from '@canvasjs/angular-charts';
import { CostumizeComponentComponent } from './costumize-component/costumize-component.component';
import { FormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    AppComponent,
    TabComponentComponent,
    UserTableComponent,
    ChartComponentComponent,
    CostumizeComponentComponent,
  ],
  imports: [
    BrowserModule,
    MatTabsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    CanvasJSAngularChartsModule,
    FormsModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
