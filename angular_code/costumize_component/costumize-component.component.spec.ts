import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostumizeComponentComponent } from './costumize-component.component';

describe('CostumizeComponentComponent', () => {
  let component: CostumizeComponentComponent;
  let fixture: ComponentFixture<CostumizeComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostumizeComponentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CostumizeComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
