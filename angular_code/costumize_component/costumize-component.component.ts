import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import * as e from 'cors';
import { timer } from 'rxjs';

@Component({
  selector: 'app-costumize-component',
  templateUrl: './costumize-component.component.html',
  styleUrls: ['./costumize-component.component.css']
})
export class CostumizeComponentComponent {

  constructor(private http: HttpClient) {}

  sample_time: string = '';// the input from the user
  sample_time_html: string = '';

  max_temp: string = '';  // the input from the user
  min_temp: string = '';  // the input from the user

  min_humid: string = ''; // the input from the user
  max_humid: string = ''; // the input from the user

  temp_min: number = 0;   // the input converted to number
  temp_max: number = 0;   // the input converted to number

  humid_min: number = 0;  // the input converted to number
  humid_max: number = 0;  // the input converted to number

  temp: number = 0;       // the input converted to number
  a: string = '/^\d+$/';  // regex for checking if the input is a number
  message: string = '';   // the message that will be displayed to the user
  message2: string = '';  // the message that will be displayed to the user
  isnum: boolean = false; // check if the input is a number
  isnum2: boolean = false;// check if the input is a number

  compare_max_temperature: number = 0;
  compare_min_temperature: number = 0;
  compare_max_humidity: number = 0;
  compare_min_humidity: number = 0;
  current_temperature: number = 0;
  current_humidity: number = 0;
  current_co2: number = 0;
  date: Date = new Date();
  messagge_compare_temperature: string = '';
  messagge_compare_humidity: string = '';
  messagge_compare_co2: string = '';


  ngOnInit(): void {
    timer(0,1000).subscribe(() => this.get_sample_time());
    timer(0,1000).subscribe(() => this.compare_temperature());
  }

  addSampleTime(sample_time_post:string): void { // add the sample time to the database
    this.http.post('http://localhost/receive_data_from_client.php',sample_time_post).subscribe();// send the sample time to the database
  }

  selected = '';// the default value of the select
  text: string = "";


  save_sample_time(): void{
    console.log(this.selected);// log the message to the console
    this.temp = Number.parseInt(this.selected);

    let isnum_temp = /^\d+$/.test(this.selected);// check if the input is a number
    this.isnum = isnum_temp;
    console.log(isnum_temp);
    console.log(this.temp);

    this.addSampleTime(JSON.stringify({ sample_time:this.selected, id:"1" }));// add the sample time to the database

  }


  set_alarm_parameters(min_max_temp:string): void{
    this.http.post('http://localhost/receive_data_from_client.php',min_max_temp).subscribe();// send the sample time to the database
    console.log("The sample time is added to the database");// log the message to the console
  }

  save_temp_parameters(): void {  // save the input from the user

    this.temp_min = Number.parseInt(this.min_temp);// convert the input to number
    this.temp_max = Number.parseInt(this.max_temp);// convert the input to number

    let isnum_temp_min = /^\d+$/.test(this.min_temp);// check if the input is a number
    let isnum_temp2_max = /^\d+$/.test(this.max_temp);// check if the input is a number
    this.isnum = isnum_temp_min && isnum_temp2_max;

    if (this.isnum) {  // if the input is a number
      if((this.temp_min < 0 || this.temp_min > 100) || (this.temp_max < 0 || this.temp_max > 100) ){// if the input is not in the range
        this.message = `The input is not in the range [0,100]°C`;
      }
      else {
        this.message = `Values added`
        this.addSampleTime(JSON.stringify({ temp_min:this.min_temp, temp_max:this.max_temp, id:"2" }));// add the sample time to the database
      };
    }
    else {  // if the input is not a number
      this.message = `The input is not a number`;
    }

  }

  save_humid_parameters(): void {  // save the input from the user
    console.log(this.min_humid);
    console.log(this.max_humid);

    this.humid_min = Number.parseInt(this.min_humid);
    this.humid_max = Number.parseInt(this.max_humid);

    let isnum_temp_min = /^\d+$/.test(this.min_humid);// check if the input is a number
    let isnum_temp2_max = /^\d+$/.test(this.max_humid);// check if the input is a number
    this.isnum2 = isnum_temp_min && isnum_temp2_max;
    console.log(isnum_temp_min);
    console.log(isnum_temp2_max);

    if (this.isnum2) {  // if the input is a number
      console.log("The input is a number");
      if((this.humid_min < 0 || this.humid_min > 100) || (this.humid_max < 0 || this.humid_max > 100) ){// if the input is not in the range

        this.message2 = `The input is not in the range [0,100]°C`;
      }
      else {
        this.message2 = `Values added`
        this.addSampleTime(JSON.stringify({ humid_min:this.min_humid, humid_max:this.max_humid, id:"3" }));// add the sample time to the database
        
        console.log(JSON.stringify({ humid_min:this.min_humid, humid_max:this.max_humid, id:"3" }))
      };
    }
    else {  // if the input is not a number
      console.log("The input is not a number");
      this.message2 = `The input is not a number`;
    }

  }

  get_sample_time(): void { // get the sample time from the database
    this.http.get('http://localhost/send_sample_time.php', { responseType: 'json' }).subscribe((response: any) => {
      let data = response;
      this.sample_time_html = data[0].sample_time;
      this.sample_time_html = String(Number(this.sample_time_html)/1000);
      console.log(this.sample_time_html);
      });
    }

    compare_temperature(): void{
    
      this.http.get('http://localhost/send_alarm_parameters.php', { responseType: 'json' }).subscribe((response: any) => {
        let data = response;
        this.compare_min_temperature = Number(data[0].temp_min);
        this.compare_max_temperature = Number(data[0].temp_max);
        this.compare_min_humidity = Number(data[0].humid_min);
        this.compare_max_humidity = Number(data[0].humid_max);
    
        });

        this.http.get('http://localhost/send_charts.php', { responseType: 'json' }).subscribe((response: any) => {
          let data = response;
            this.current_temperature = Number(data[data.length-1].temperatura);
            this.current_humidity = Number(data[data.length-1].umiditate);
            this.date = new Date(data[data.length-1].timp);
            this.current_co2 = Number(data[data.length-1].co2);
            
        });

            console.log(this.current_temperature);
            console.log(this.current_humidity);
            console.log(this.date);
            console.log(this.current_co2);

        if(this.current_temperature < this.compare_min_temperature){
          console.log("The temperature is too low");
          this.messagge_compare_temperature = "The temperature is too low: " + this.current_temperature + "°C" ;
        }
        else if(this.current_temperature > this.compare_max_temperature){
          console.log("The temperature is too high");
          this.messagge_compare_temperature = "The temperature is too high: " + this.current_temperature + "°C";
        }
        else{
          console.log("The temperature is ok");
          this.messagge_compare_temperature = "The temperature is ok: " + this.current_temperature + "°C";
        }

        if(this.current_humidity < this.compare_min_humidity){
          console.log("The humidity is too low");
          this.messagge_compare_humidity = "The humidity is too low: " + this.current_humidity + "%";
        }
        else if(this.current_humidity > this.compare_max_humidity){
          console.log("The humidity is too high");
          this.messagge_compare_humidity = "The humidity is too high: " + this.current_humidity + "%";
        }
        else{
          console.log("The humidity is ok");
          this.messagge_compare_humidity = "The humidity is ok: " + this.current_humidity + "%";
        }

        if(this.current_co2 > 1000){
          console.log("The CO2 concentration is too high:");
          this.messagge_compare_co2 = "The co2 is too high: " + this.current_co2 + "ppm";
        }
        else{
          console.log("The CO2 concentration is ok");
          this.messagge_compare_co2 = "The co2 is ok: " + this.current_co2 + "ppm";
        }

      }



}
