
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>   // Universal Telegram Bot Library written by Brian Lough: https://github.com/witnessmenow/Universal-Arduino-Telegram-Bot
#include <ArduinoJson.h>

#include <Wire.h>

#include "DHT.h"
#define DHTTYPE DHT11
uint8_t DHTPin = D5;
DHT dht(DHTPin, DHTTYPE);
float Temperature;
float Humidity;
int Sample_time;
int Sample_time2;
char temp[5];
int carbon;
String inString = "";


#define serverName "http://192.168.0.101/receive_data.php"
#define Post_sample_time_php "http://192.168.0.101/receive_sample_time.php"
#define Get_sample_time_php "http://192.168.0.101/send_sample_time_to_NodeMCU.php"
String apiKeyValue = "baban";

#ifndef STASSID
#define STASSID "Arax54"
#define STAPSK "079405546"
#endif
// Initialize Telegram BOT
#define BOTtoken "6066242286:AAF7KA3xn86RlaTFl7P6dNO6DHI6rGqQMEs" // your Bot Token (Get from Botfather)
#define CHAT_ID "743395305"

#ifdef ESP8266
  X509List cert(TELEGRAM_CERTIFICATE_ROOT);
#endif

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

int botRequestDelay = 5000;
unsigned long lastTimeBotRan;
unsigned long last_sample_time_sent;
unsigned long last_sample_time_received;

unsigned long previousMillis = 0;
unsigned long interval = 30000;

void setup() {

  Wifi_Init(STASSID,STAPSK);
  Sensors_Init(DHTPin);
  Wire.begin(D1, D2);

  #ifdef ESP8266
    configTime(0, 0, "pool.ntp.org");      // get UTC time via NTP
    client.setTrustAnchors(&cert); // Add root certificate for api.telegram.org
  #endif


  Sample_time = 15000; // default initial sample time

}

void loop() {
  unsigned long currentTime = millis();

  Temperature = dht.readTemperature(); // primestea valoarea temperaturii
  Humidity = dht.readHumidity(); // primestea valoarea umiditatii

  if (currentTime > lastTimeBotRan + botRequestDelay)  {
    Serial.println("bot");
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

  while(numNewMessages) {
    Serial.println("got response");
    handleNewMessages(numNewMessages);
    numNewMessages = bot.getUpdates(bot.last_message_received + 1);
  }
    lastTimeBotRan = currentTime;
  }

  if (currentTime > last_sample_time_received + botRequestDelay)  {
    Serial.println("get");
    Sample_time = Wifi_Get_sample_time();
    Serial.println(Sample_time);
    last_sample_time_received = currentTime;
  }

  if(currentTime > last_sample_time_sent + Sample_time ){
    //Serial.println("post");
  Wire.beginTransmission(8); /* begin with device address 8 */
  Wire.endTransmission();    /* stop transmitting */

  Wire.requestFrom(8, 5); /* request & read data of size 13 from slave */
  int i=0;
  inString = "";

  while(Wire.available()){
      //char c = Wire.read();
      temp[i] = Wire.read();
      
      if(isDigit(temp[i])){
        inString += temp[i];
      }
      i++;
    //Serial.print(c);
  }
    carbon = inString.toInt();
    Serial.println("post");
    Wifi_Post_data(Temperature,Humidity,carbon);

    last_sample_time_sent = currentTime;
  }

  if(Sample_time < 1000 || Sample_time > 60000){
    Sample_time = 15000;
  } 

    //print the Wi-Fi status every 30 seconds
  // unsigned long currentMillis = millis();
  // if (currentMillis - previousMillis >=interval){
  //   switch (WiFi.status()){
  //     case WL_NO_SSID_AVAIL:
  //       Serial.println("Configured SSID cannot be reached");
  //       break;
  //     case WL_CONNECTED:
  //       Serial.println("Connection successfully established");
  //       break;
  //     case WL_CONNECT_FAILED:
  //       Serial.println("Connection failed");
  //       break;
  //   }
  //   Serial.printf("Connection status: %d\n", WiFi.status());
  //   Serial.print("RRSI: ");
  //   Serial.println(WiFi.RSSI());
  //   previousMillis = currentMillis;
  // }
  //delay(100);

}




void Wifi_Init(String Wifi_name, String Wifi_password){

  Serial.begin(115200);// baud rate for serial monitor
  Serial.println();
  Serial.println();
  Serial.println("first");

  WiFi.begin(Wifi_name, Wifi_password);// connection to wifi with wifi credentials

  while (WiFi.status() != WL_CONNECTED) {// if not yet connected
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());

  WiFi.setAutoReconnect(true);
  WiFi.persistent(true);

}

void Wifi_Post_data(float Temperature, float Humidity, int CO2){
  int sample_time_temp;

  // wait for WiFi connection
  if ((WiFi.status() == WL_CONNECTED)) {
    WiFiClient client;
    HTTPClient http;
    // configure traged server and url
    http.begin(client,serverName);  // HTTP
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    Serial.print("[HTTP] POST...\n");
    // start connection and send HTTP header and body

    String httpRequestData = "api_key=" + apiKeyValue + "&temperaturaa=" + Temperature + "&umiditatee=" + Humidity + "&co22=" + CO2 + "";

    // Send HTTP POST request
    int httpResponseCode = http.POST(httpRequestData);

    // httpCode will be negative on error
    if (httpResponseCode > 0) {
       //HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpResponseCode);
      const String& payload = http.getString();
      sample_time_temp = payload.toInt();
        Serial.println("received payload:\n<<");
        Serial.println(payload);
        Serial.println(">>");
    } else {
      Serial.printf("[HTTP] POST_data... failed, error: %s\n", http.errorToString(httpResponseCode).c_str());
    }
    http.end();
  }

}

void Wifi_Post_sample_time(int Sample_time){

  // wait for WiFi connection
  if ((WiFi.status() == WL_CONNECTED)) {

    WiFiClient client;
    HTTPClient http;

    Serial.print("[HTTP] begin...\n");

    // configure traged server and url
    http.begin(client,Post_sample_time_php);  // HTTP
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    Serial.print("[HTTP] POST...\n");
    // start connection and send HTTP header and body

    //int httpCode = http.POST("{\"hello\":\"world\"}");
    String httpRequestData = "api_key=" + apiKeyValue + "&sample_time=" + Sample_time + "";
    Serial.print("httpRequestData: ");
    Serial.println(httpRequestData);

    // Send HTTP POST request
    int httpResponseCode = http.POST(httpRequestData);

    // httpCode will be negative on error
    if (httpResponseCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpResponseCode);
      const String& payload = http.getString();
      //sample_time_temp = payload.toInt();
        //Serial.println("received payload:\n<<");
        //Serial.println(payload);
        //Serial.println("--->",sample_time_temp);
        //Serial.println(">>");
    } else {
      Serial.printf("[HTTP] POST_sample_time... failed, error: %s\n", http.errorToString(httpResponseCode).c_str());
    }

    http.end();
  }

}

int Wifi_Get_sample_time(){
  int sample_time_temp;
  // wait for WiFi connection
  if ((WiFi.status() == WL_CONNECTED)) {

    WiFiClient client;
    HTTPClient http;

    //Serial.print("[HTTP] begin...\n");

    // configure traged server and url
    http.begin(client,Get_sample_time_php);  // HTTP
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    //Serial.print("[HTTP] GET...\n");
    // start connection and send HTTP header and body
    String id_value = "10";
    String httpRequestData = "id=" + id_value + "";
    //Serial.print("httpRequestData: ");
    //Serial.println(httpRequestData);

    // Send HTTP POST request
    int httpResponseCode = http.POST(httpRequestData);

    // httpCode will be negative on error
    if (httpResponseCode > 0) {
      // HTTP header has been send and Server response header has been handled
      //Serial.printf("[HTTP] GET... code: %d\n", httpResponseCode);
      const String& payload = http.getString();
      sample_time_temp = payload.toInt();
        Serial.println("received payload:\n<<");
        Serial.println(payload);
        //Serial.println("--->",sample_time_temp);
        Serial.println(">>");
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpResponseCode).c_str());
    }

    http.end();
  }

  return sample_time_temp;

}


void Sensors_Init(uint8 DHTPin){
  pinMode(DHTPin, INPUT);
  dht.begin();
}


void Sensors_Read(float temperature, float humidity, int co2){
  temperature = dht.readTemperature(); // primestea valoarea temperaturii
  humidity = dht.readHumidity();     // primestea valoarea umiditatii
}

// Handle what happens when you receive new messages
void handleNewMessages(int numNewMessages) {
  Serial.println("handleNewMessages");
  Serial.println(String(numNewMessages));

  for (int i=0; i<numNewMessages; i++) {
    // Chat id of the requester
    String chat_id = String(bot.messages[i].chat_id);
    if (chat_id != CHAT_ID){
      bot.sendMessage(chat_id, "Unauthorized user", "");
      continue;
    }
    
    // Print the received message
    String text = bot.messages[i].text;
    Serial.println(text);

    String from_name = bot.messages[i].from_name;

    if (text == "/start") {
      String welcome = "Welcome, " + from_name + ".\n";
      welcome += "Use the following commands to control your board.\n\n";
      welcome += "/set_sample_time to change sample time \n";
      welcome += "/state to request current temperatura an humidty status \n";
      bot.sendMessage(chat_id, welcome, "");
    }

    if (text == "/set_sample_time") {
      bot.sendMessage(chat_id, "Choose the sample time", "");
      String sample_time = " ";
      sample_time += "/5_seconds\n";
      sample_time += "/10_seconds\n";
      sample_time += "/15_seconds\n";
      sample_time += "/30_seconds\n";
      sample_time += "/60_seconds\n";
      sample_time += "/return \n";
      bot.sendMessage(chat_id, sample_time, "");
    }

    if(text == "/5_seconds") {
      int sample_time_temp = 5;
      Wifi_Post_sample_time(sample_time_temp);
      bot.sendMessage(chat_id, "Sample time is 5 seconds", "");
    }
    if(text == "/10_seconds") {
      int sample_time_temp = 10;
      Wifi_Post_sample_time(sample_time_temp);
      bot.sendMessage(chat_id, "Sample time is 10 seconds", "");
    }
    if(text == "/15_seconds") {
      int sample_time_temp = 15;
      Wifi_Post_sample_time(sample_time_temp);
      bot.sendMessage(chat_id, "Sample time is 15 seconds", "");
    }
    if(text == "/30_seconds") {
      int sample_time_temp = 30;
      Wifi_Post_sample_time(sample_time_temp);
      bot.sendMessage(chat_id, "Sample time is 30 seconds", "");
    }
    if(text == "/60_seconds") {
      int sample_time_temp = 60;
      Wifi_Post_sample_time(sample_time_temp);
      bot.sendMessage(chat_id, "Sample time is 60 seconds", "");
    }
    
    if (text == "/state") {
      String data = "Temperature: " + String(Temperature) + " ºC \n";
      data += "Humidity: " + String(Humidity) + " % \n";
      data += "CO2 concentration: " + String(carbon) + " ppm\n";

      bot.sendMessage(chat_id, data, "");
    }

  }
}
