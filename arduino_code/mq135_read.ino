#include <Wire.h>

#include <MQ135.h> 


#define analogPin A0 
MQ135 gasSensor = MQ135(analogPin); 

void setup() {
  Wire.begin(8);                /* join i2c bus with address 8 */
  Wire.onReceive(receiveEvent); /* register receive event */
  Wire.onRequest(requestEvent);
  Serial.begin(115200); 
  delay(1000); 
}

void loop() {
  delay(1000); 
}

void receiveEvent(int howMany) {
 while (0 <Wire.available()) {
    char c = Wire.read();      /* receive byte as a character */
    Serial.print(c);           /* print the character */
  }
 Serial.println();             /* to newline */
}

// function that executes whenever data is requested from master
void requestEvent() {

  int ppm = gasSensor.getPPM();
  char buff[32];
  String toSend;
  String text = String(ppm);
  text.toCharArray(buff, 32);

  Wire.write(buff);
}
