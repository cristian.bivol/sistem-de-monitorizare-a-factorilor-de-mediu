# Sistem de monitorizare a factorilor de mediu

https://gitlab.upt.ro/cristian.bivol/sistem-de-monitorizare-a-factorilor-de-mediu


Folderul arduino_code:
    fisierul posthttpclient trebuie incarcat pe placa NodeMCU
    fisierul mq135 trebuie incarcat pe arduino

Folderul php_code:
    fisierele trebuie puse intr-un folder pe PC si calea folderului indicata in Apache web server,
    desemenea in fisierul conect trebuie indicata adresa ip a bazei de date
    trebuie instalata o baza de date MariaDB si configurata respectiv

Folderul angular_code:
    trebuie de creat un proiect in angular
    folderele trebuie aplasate in folerul proiectului numit 'src'

Proiectul functioneaza daca baza de date e disponibila pe server, partea de hardware functioneaz si siteul web e pornit din visual studio code
